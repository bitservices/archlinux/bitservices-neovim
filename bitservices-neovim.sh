#!/bin/bash -e
################################################################################

hash cp
hash tee
hash echo
hash mkdir

################################################################################

BITSERVICES_NEOVIM_CONFIG_BASE="${HOME}/.config/nvim"

################################################################################

BITSERVICES_NEOVIM_INIT_LUA="${BITSERVICES_NEOVIM_CONFIG_BASE}/init.lua"
BITSERVICES_NEOVIM_INIT_VIM="${BITSERVICES_NEOVIM_CONFIG_BASE}/init.vim"

################################################################################

BITSERVICES_NEOVIM_INIT_LUA_BACKUP="${BITSERVICES_NEOVIM_INIT_LUA}.bsnbak"
BITSERVICES_NEOVIM_INIT_VIM_BACKUP="${BITSERVICES_NEOVIM_INIT_VIM}.bsnbak"

################################################################################

BITSERVICES_NEOVIM_LUA_PATH="${BITSERVICES_NEOVIM_CONFIG_BASE}/lua"
BITSERVICES_NEOVIM_SNIPPETS_PATH="${BITSERVICES_NEOVIM_CONFIG_BASE}/snippets"
BITSERVICES_NEOVIM_SYNTAX_PATH="${BITSERVICES_NEOVIM_CONFIG_BASE}/syntax"

################################################################################

BITSERVICES_NEOVIM_LUA_BITSERVICES_PATH="${BITSERVICES_NEOVIM_LUA_PATH}/bitservices"

################################################################################

echo "Setting up BITServices Neovim configuration for user: ${USER}"

################################################################################

if [ ! -d "${BITSERVICES_NEOVIM_CONFIG_BASE}" ]; then
  echo " - Creating folder: ${BITSERVICES_NEOVIM_CONFIG_BASE}"
  mkdir "${BITSERVICES_NEOVIM_CONFIG_BASE}"
fi

if [ ! -d "${BITSERVICES_NEOVIM_LUA_PATH}" ]; then
  echo " - Creating folder: ${BITSERVICES_NEOVIM_LUA_PATH}"
  mkdir "${BITSERVICES_NEOVIM_LUA_PATH}"
fi

if [ ! -d "${BITSERVICES_NEOVIM_SNIPPETS_PATH}" ]; then
  echo " - Creating folder: ${BITSERVICES_NEOVIM_SNIPPETS_PATH}"
  mkdir "${BITSERVICES_NEOVIM_SNIPPETS_PATH}"
fi

if [ ! -d "${BITSERVICES_NEOVIM_SYNTAX_PATH}" ]; then
  echo " - Creating folder: ${BITSERVICES_NEOVIM_SYNTAX_PATH}"
  mkdir "${BITSERVICES_NEOVIM_SYNTAX_PATH}"
fi

if [ ! -d "${BITSERVICES_NEOVIM_LUA_BITSERVICES_PATH}" ]; then
  echo " - Creating folder: ${BITSERVICES_NEOVIM_LUA_BITSERVICES_PATH}"
  mkdir "${BITSERVICES_NEOVIM_LUA_BITSERVICES_PATH}"
fi

chmod 700 "${BITSERVICES_NEOVIM_CONFIG_BASE}"
chmod 700 "${BITSERVICES_NEOVIM_LUA_PATH}"
chmod 700 "${BITSERVICES_NEOVIM_SNIPPETS_PATH}"
chmod 700 "${BITSERVICES_NEOVIM_SYNTAX_PATH}"
chmod 700 "${BITSERVICES_NEOVIM_LUA_BITSERVICES_PATH}"

################################################################################

if [ -f "${BITSERVICES_NEOVIM_INIT_LUA}" ]; then
  if [ -f "${BITSERVICES_NEOVIM_INIT_LUA_BACKUP}" ]; then
    echo "Backup file: ${BITSERVICES_NEOVIM_INIT_LUA_BACKUP} already exists!"
    echo "Please remove file manually or store it with a different name and run this script again."
    exit 1
  fi

  echo " - Backing up config: ${BITSERVICES_NEOVIM_INIT_LUA}"
  cp -v "${BITSERVICES_NEOVIM_INIT_LUA}" "${BITSERVICES_NEOVIM_INIT_LUA_BACKUP}"
fi

################################################################################

if [ -f "${BITSERVICES_NEOVIM_INIT_VIM}" ]; then
  if [ -f "${BITSERVICES_NEOVIM_INIT_VIM_BACKUP}" ]; then
    echo "Backup file: ${BITSERVICES_NEOVIM_INIT_VIM_BACKUP} already exists!"
    echo "Please remove file manually or store it with a different name and run this script again."
    exit 1
  fi

  echo " - Backing up config: ${BITSERVICES_NEOVIM_INIT_VIM}"
  mv -v "${BITSERVICES_NEOVIM_INIT_VIM}" "${BITSERVICES_NEOVIM_INIT_VIM_BACKUP}"
fi

################################################################################

echo " - Writing config: ${BITSERVICES_NEOVIM_INIT_LUA}"

tee "${BITSERVICES_NEOVIM_INIT_LUA}" <<INITLUA
--------------------------------------------------------------------------------

dofile('/usr/share/bitservices-neovim/bitservices.lua')

--------------------------------------------------------------------------------
INITLUA


################################################################################

echo "Done!"

################################################################################
