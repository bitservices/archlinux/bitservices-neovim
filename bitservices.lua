-------------------------------------------------------------------------------

local bitservicesPath = '/usr/share/bitservices-neovim'
local runtimePath     = vim.o.runtimepath

if (runtimePath == nil or runtimePath == '') then
  runtimePath = bitservicesPath
else
  runtimePath = runtimePath .. ',' .. bitservicesPath
end

vim.o.runtimepath = runtimePath

-------------------------------------------------------------------------------

require('bitservices.basic')
require('bitservices.golang')
require('bitservices.helm')

-------------------------------------------------------------------------------

require('lazy').setup({
  spec = {
    {
      import = 'bitservices.plugins'
    }
  },
  change_detection = {
    enabled = false
  },
  checker = {
    enabled = false
  },
  defaults = {
    local_spec = false
  },
  performance = {
    rtp = {
      paths = {
        bitservicesPath
      }
    }
  }
})

-------------------------------------------------------------------------------

require('bitservices.project')

-------------------------------------------------------------------------------

vim.api.nvim_create_autocmd(
  {
    'UIEnter'
  },
  {
    pattern  = '*',
    callback = function()
      require('bitservices.gui')
    end
  }
)

-------------------------------------------------------------------------------
