--------------------------------------------------------------------------------
--- PKGBUILD requires: ttf-firacode-nerd
--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'linrongbin16/lsp-progress.nvim',
    config = true
  },

  ------------------------------------------------------------------------------

  {
    'nvim-lualine/lualine.nvim',
    dependencies = {
      'linrongbin16/lsp-progress.nvim',
      'nvim-tree/nvim-web-devicons'
    },
    config = function(_, opts)
      require('lualine').setup(opts)

      --------------------------------------------------------------------------

      vim.api.nvim_create_augroup('lualine', {
        clear = false
      })

      --------------------------------------------------------------------------

      vim.api.nvim_create_autocmd("User", {
        group    = 'lualine',
        pattern  = 'LspProgressStatusUpdated',
        callback = function() require('lualine').refresh() end
      })
    end,
    opts = {
      inactive_winbar = {
        lualine_b = { 'filename' }
      },

      options = {
        globalstatus = true
      },

      sections = {
        lualine_c = { function()
          return require('lsp-progress').progress()
        end
        },

        lualine_x = {},
        lualine_y = { 'encoding', 'fileformat', 'filetype' },
        lualine_z = { 'filesize', 'progress', 'location' }
      },

      winbar = {
        lualine_b = { 'filename' },
        lualine_y = { 'selectioncount', 'searchcount' }
      }
    }
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
