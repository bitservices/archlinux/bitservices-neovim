--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'dcampos/nvim-snippy',
      'hrsh7th/cmp-nvim-lsp',
      'dcampos/cmp-snippy'
    },
    event = 'InsertEnter',
    opts = function()
      local cmp = require('cmp')

      return {
        mapping = cmp.mapping.preset.insert({
          ['<C-e>']     = cmp.mapping.abort(),
          ['<C-E>']     = cmp.mapping.abort(),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<CR>']      = cmp.mapping.confirm({ select = true })
        }),

        snippet = {
          expand = function(args)
            require('snippy').expand_snippet(args.body)
          end
        },

        sources = cmp.config.sources({
          {
            name = 'nvim_lsp'
          },
          {
            name = 'snippy'
          },
          {
            name = 'buffer'
          }
        }),

        window = {}
      }
    end
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
