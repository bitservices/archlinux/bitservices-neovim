--------------------------------------------------------------------------------

function bitservices_project_switch(project)
  if (project == nil or project == '') then
    error('A valid project name must be specified!')
  end

  if require('lazy.core.config').plugins['nvim-tree.lua']._.loaded ~= nil then
    require('nvim-tree.api').tree.close()
  end

  local buffers = vim.api.nvim_list_bufs()

  for _, buffer in ipairs(buffers) do
    if vim.api.nvim_buf_is_valid(buffer) then
      vim.api.nvim_buf_delete(buffer, { force = false, unload = false })
    end
  end

  vim.api.nvim_set_current_dir('~/Automation/' .. project)

  if require('lazy.core.config').plugins['fzf-lua']._.loaded == nil then
    require('lazy').load({ plugins = { 'fzf-lua' } })
  end

  require('fzf-lua').files()
end

--------------------------------------------------------------------------------

vim.api.nvim_create_user_command(
  'Proj',
  function(input)
    bitservices_project_switch(input.args)
  end,
  { nargs = 1 }
)

--------------------------------------------------------------------------------

pcall(require, 'bitservices.project-keymaps')

--------------------------------------------------------------------------------
-- Keybindings can be set in local configuration. A file should be created
-- called "project-keymaps.lua" in a folder called "bitservices" that is inside
-- the local configuration "lua" folder.
--
-- Example: ~/.config/nvim/lua/bitservices/project-keymaps.lua
--
-- vim.keymap.set("", "<Leader>pb", function() bitservices_project_switch('bitservices') end)
-- vim.keymap.set("", "<Leader>PB", function() bitservices_project_switch('bitservices') end)
--------------------------------------------------------------------------------
