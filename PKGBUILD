# Maintainer: Richard Lees <git zero at bitservices dot io>
################################################################################

pkgname=bitservices-neovim
pkgver=0.4.0
pkgrel=6
pkgdesc="BITServices Neovim Configuration"
install="${pkgname}.install"
arch=('any')
license=('none')
depends=('bat' 'fd' 'fzf' 'git-delta' 'glow' 'lazygit' 'neovim' 'nvim-lazy' 'ripgrep' 'ttf-firacode-nerd' 'viu' 'wl-clipboard')
optdepends=('bash-language-server: Bash language server support'
            'bicep-langserver: Bicep language server support'
            'dockerfile-language-server-bin: Dockerfile language server support'
            'eslint-language-server: ESLint language server support'
            'gopls: Golang language server support'
            'helm-ls-bin: Helm language server support'
            'lua-language-server: LUA language server support'
            'php-cs-fixer: PHP auto formatting, must be installed if using phpactor'
            'phpactor: PHP language server support'
            'powershell-editor-services: Powershell language server support'
            'python-lsp-server: Python language server support'
            'ruby-solargraph: Ruby language server support'
            'terraform-ls-bin: Terraform language server support'
            'vscode-json-languageserver: JSON language server support'
            'vtsls: Typescript language server support'
            'yaml-language-server: YAML language server support')
source=("bitservices-neovim.sh"
        "bitservices.lua"
        "basic.lua"
        "golang.lua"
        "gui.lua"
        "helm.lua"
        "helm.syntax"
        "plugin-cmp.lua"
        "plugin-fzf.lua"
        "plugin-glow.lua"
        "plugin-lazygit.lua"
        "plugin-lspconfig.lua"
        "plugin-lualine.lua"
        "plugin-mustache-handlebars.lua"
        "plugin-nvim-tree.lua"
        "plugin-onedark.lua"
        "plugin-snippy.lua"
        "plugin-trouble.lua"
        "project.lua"
        "terraform.snippets"
        "yaml.snippets")
sha256sums=('d32fd3cb8fd6a0a70d0079742012ade4adc1125daa868c658d9113e4f47cdfcb'
            'c09855595cdf634e4a381e72c0add58e864fb8f046dd3583b893aac94eb33380'
            '1c964f15180e39fecb1a2769a418d093aca0a1cf7d087791d3b4785771aacd52'
            'b057559839f20ff192547de5375aa27c49002730125da33c5400d59047bf8c75'
            'a87a65367a3e1122950dc4d77d96226a52e62163b7e9d8c0512b66cc84731ab6'
            '2f518784edc538a8c6d8e8afdfa274aa60eabaf6958af805e2811c48e9d01d20'
            '8f29cba837993508eae907daea6b98dfd63b317ef285dc1c63bf1f408850d900'
            '49cbe344a2f3d62e74e687d5579263e2d16abdf4ce8d1ffa64ab73163ad3bde9'
            '6afffd70241ccde086617d86673107fdafec45c6f3cb49c4ce14dfcc455c9020'
            '4b21b0253feb47a8a679076955a6f7854e9847c496f7f9f82ce4e5947fde563e'
            '7991966a8d09e313354270f5b14e0b3912a646ac59f44d61183754ff5d005625'
            '5db33c38ce8259dbe81059efdd3b0c3c17cdb6b8b2febb6dea5f0388783a14bf'
            'f9a1a64c58d8e74356ae1a325911a6ad53bd77ce151390378a0003678bf9ade0'
            'd31847da4a4f4e1c67ad1b7b3fabf3e01027da7285a01c4171560605c70b747e'
            'bf0360e7c2f6b203c6f97eacf70c11df98db07c51f45e32d45857ec16abfdca9'
            '26eb05e21ddd544a4c4f2a9ab949b3e697d118812c93ea8cc1c212c5cc0188dd'
            'cfc14af77774cf56846ea45005bec49e58e7ddb0516fa391500f7a5498353e2d'
            '569c0ea5d50aba5cbe86ab15c554686680ddb4a64c9c0fd4576da3957d70596d'
            'bbdc664845a6f4a0d21ff01a03ce7f6ffee784c5ed0d668ebf559036b84dcc0c'
            'd33be65fb7707547533753c503f9525e86b1f233a05b18d721c993ce26eb9157'
            'eb3f0a83f04742867570d5a1fa72dff2d13eeb54055e50fbce15f050120dce73')

################################################################################

package() {
  install -d "${pkgdir}/usr/bin"
  install -d "${pkgdir}/usr/share/${pkgname}"
  install -d "${pkgdir}/usr/share/${pkgname}/lua"
  install -d "${pkgdir}/usr/share/${pkgname}/lua/bitservices"
  install -d "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins"
  install -d "${pkgdir}/usr/share/${pkgname}/snippets"
  install -d "${pkgdir}/usr/share/${pkgname}/syntax"

  install -m755 "${srcdir}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"

  install -m644 "${srcdir}/bitservices.lua" "${pkgdir}/usr/share/${pkgname}/bitservices.lua"

  install -m644 "${srcdir}/basic.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/basic.lua"
  install -m644 "${srcdir}/golang.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/golang.lua"
  install -m644 "${srcdir}/gui.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/gui.lua"
  install -m644 "${srcdir}/helm.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/helm.lua"
  install -m644 "${srcdir}/project.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/project.lua"

  install -m644 "${srcdir}/plugin-cmp.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/cmp.lua"
  install -m644 "${srcdir}/plugin-fzf.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/fzf.lua"
  install -m644 "${srcdir}/plugin-glow.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/glow.lua"
  install -m644 "${srcdir}/plugin-lazygit.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/lazygit.lua"
  install -m644 "${srcdir}/plugin-lspconfig.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/lspconfig.lua"
  install -m644 "${srcdir}/plugin-lualine.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/lualine.lua"
  install -m644 "${srcdir}/plugin-mustache-handlebars.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/mustache-handlebars.lua"
  install -m644 "${srcdir}/plugin-nvim-tree.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/nvim-tree.lua"
  install -m644 "${srcdir}/plugin-onedark.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/onedark.lua"
  install -m644 "${srcdir}/plugin-snippy.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/snippy.lua"
  install -m644 "${srcdir}/plugin-trouble.lua" "${pkgdir}/usr/share/${pkgname}/lua/bitservices/plugins/trouble.lua"

  install -m644 "${srcdir}/terraform.snippets" "${pkgdir}/usr/share/${pkgname}/snippets/terraform.snippets"
  install -m644 "${srcdir}/yaml.snippets" "${pkgdir}/usr/share/${pkgname}/snippets/yaml.snippets"

  install -m644 "${srcdir}/helm.syntax" "${pkgdir}/usr/share/${pkgname}/syntax/helm.vim"
}

################################################################################
