--------------------------------------------------------------------------------
--- PKGBUILD requires: bat, fd, fzf, git-delta, ripgrep, ttf-firacode-nerd, viu
--------------------------------------------------------------------------------

local function Bitservices_FZF_GitPath()
  local localpath
  local pathspec = vim.fn.expand('%:p:h') .. ';' .. vim.fn.getcwd()
  local gitpath = vim.fn.finddir('.git', pathspec)

  if (gitpath == nil or gitpath == '') then
    localpath = vim.fn.expand('%:p:h')
  else
    localpath = vim.fn.fnamemodify(gitpath, ':h')
  end

  return localpath
end

--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'ibhagwan/fzf-lua',
    dependencies = {
      'nvim-tree/nvim-web-devicons'
    },
    keys = {
      { '<Leader>b', function() require('fzf-lua').buffers({ cwd = Bitservices_FZF_GitPath() }) end },
      { '<Leader>B', function() require('fzf-lua').buffers() end },

      { '<Leader>f', function() require('fzf-lua').files({ cwd = Bitservices_FZF_GitPath() }) end },
      { '<Leader>F', function() require('fzf-lua').files() end },

      { '<Leader>t', function() require('fzf-lua').live_grep({ cwd = Bitservices_FZF_GitPath() }) end },
      { '<Leader>T', function() require('fzf-lua').live_grep() end },

      { '<Leader>r', function() require('fzf-lua').lsp_finder({ cwd = Bitservices_FZF_GitPath() }) end },
      { '<Leader>R', function() require('fzf-lua').lsp_finder({ cwd = Bitservices_FZF_GitPath() }) end }
    },
    opts = function()
      local fzf_actions = require('fzf-lua.actions')

      return {
        actions = {
          files = {
            false,
            ['enter']  = fzf_actions.file_edit_or_qf,
            ['ctrl-n'] = fzf_actions.file_split,
            ['ctrl-v'] = fzf_actions.file_vsplit,
            ['ctrl-t'] = fzf_actions.file_tabedit
          }
        },
        previewers = {
          builtin = {
            extensions = {
              ['jpg'] = { 'viu', '-b', '-t' },
              ['gif'] = { 'viu', '-b', '-t' },
              ['png'] = { 'viu', '-b', '-t' }
            }
          }
        }
      }
    end
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
