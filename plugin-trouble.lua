--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'folke/trouble.nvim',
    cmd = 'Trouble',
    keys = {
      { '<Leader>dr', function() require("trouble").toggle({ mode = 'lsp', focus = false, follow = true }) end },
      { '<Leader>DR', function() require("trouble").toggle({ mode = 'lsp', focus = false, follow = true }) end },

      { '<Leader>ds', function() require("trouble").toggle({ mode = 'symbols', focus = true }) end },
      { '<Leader>DS', function() require("trouble").toggle({ mode = 'symbols', focus = true }) end }
    },
    opts = {
      follow = false,
      multiline = false,
      modes = {
        lsp = {
          win = {
            position = 'right',
            size = 0.3
          }
        },
        symbols = {
          win = {
            size = 0.3
          }
        }
      }
    }
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
