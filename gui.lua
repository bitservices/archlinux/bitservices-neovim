--------------------------------------------------------------------------------

local isFvim = vim.g.fvim_loaded ~= nil and vim.g.fvim_loaded
local isNeovide = vim.g.neovide ~= nil and vim.g.neovide
local isNvimQt = vim.g.GuiLoaded ~= nil and vim.g.GuiLoaded ~= 0
local isGui = isFvim or isNeovide or isNvimQt

--------------------------------------------------------------------------------

if isGui then
  vim.opt.guifont = 'FiraCode Nerd Font Mono:h11'

  vim.api.nvim_create_autocmd(
    {
      'FocusGained'
    },
    {
      pattern  = '*',
      callback = function()
        vim.api.nvim_command('checktime')
      end
    }
  )
end

--------------------------------------------------------------------------------

if isNvimQt then
  vim.cmd('GuiRenderLigatures 1')
end

--------------------------------------------------------------------------------
