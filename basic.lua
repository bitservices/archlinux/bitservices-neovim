--------------------------------------------------------------------------------

vim.loader.enable()

--------------------------------------------------------------------------------

vim.opt.autoread     = true
vim.opt.clipboard    = 'unnamedplus'
vim.opt.cursorcolumn = true
vim.opt.expandtab    = true
vim.opt.shiftwidth   = 2
vim.opt.mouse        = 'a'
vim.opt.spelllang    = 'en_gb'
vim.opt.title        = true
vim.opt.undofile     = true
vim.opt.wildmenu     = true
vim.opt.wrap         = false

--------------------------------------------------------------------------------

vim.keymap.set('n', '<Tab>', '<CMD>tabnext<CR>')
vim.keymap.set('n', '<S-Tab>', '<CMD>tabprev<CR>')

--------------------------------------------------------------------------------

vim.keymap.set('x', 'p', 'P')
vim.keymap.set('x', 'P', 'p')

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>c', function() vim.opt.cursorcolumn = not vim.opt.cursorcolumn:get() end)
vim.keymap.set('', '<Leader>C', function() vim.opt.cursorcolumn = not vim.opt.cursorcolumn:get() end)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>s', function() vim.opt.spell = not vim.opt.spell:get() end)
vim.keymap.set('', '<Leader>S', function() vim.opt.spell = not vim.opt.spell:get() end)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>w', function() vim.opt.wrap = not vim.opt.wrap:get() end)
vim.keymap.set('', '<Leader>W', function() vim.opt.wrap = not vim.opt.wrap:get() end)

--------------------------------------------------------------------------------
