--------------------------------------------------------------------------------

vim.api.nvim_create_autocmd(
  {
    'FileType'
  },
  {
    pattern  = 'go',
    callback = function()
      vim.opt_local.expandtab  = false
      vim.opt_local.shiftwidth = 0
    end
  }
)

--------------------------------------------------------------------------------
