--------------------------------------------------------------------------------
--- PKGBUILD requires: bash-language-server, bicep-langserver,
---                    dockerfile-language-server-bin, eslint-language-server,
---                    gopls, helm-ls-bin, lua-language-server, php-cs-fixer,
---                    phpactor, powershell-editor-services, python-lsp-server,
---                    ruby-solargraph, terraform-ls-bin,
---                    vscode-json-languageserver, vtsls, yaml-language-server
--------------------------------------------------------------------------------

vim.diagnostic.config({
  virtual_text = false
})

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>DE', vim.diagnostic.enable)
vim.keymap.set('', '<Leader>de', vim.diagnostic.enable)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>DD', vim.diagnostic.disable)
vim.keymap.set('', '<Leader>dd', vim.diagnostic.disable)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>DO', vim.diagnostic.open_float)
vim.keymap.set('', '<Leader>do', vim.diagnostic.open_float)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>D[', vim.diagnostic.goto_prev)
vim.keymap.set('', '<Leader>d[', vim.diagnostic.goto_prev)

--------------------------------------------------------------------------------

vim.keymap.set('', '<Leader>D]', vim.diagnostic.goto_next)
vim.keymap.set('', '<Leader>d]', vim.diagnostic.goto_next)

--------------------------------------------------------------------------------

vim.api.nvim_create_autocmd(
  {
    'BufWritePre'
  },
  {
    pattern  = '*',
    callback = function()
      vim.lsp.buf.format()
    end
  }
)

--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'neovim/nvim-lspconfig',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp'
    },
    event = {
      'BufNewFile',
      'BufReadPost'
    },
    config = function(_, _)
      local lsp_capabilities      = require('cmp_nvim_lsp').default_capabilities()
      local lsp_util              = require('lspconfig').util

      --------------------------------------------------------------------------

      local lsp_bicep_binary      = 'Bicep.LangServer.dll'
      local lsp_bicep_bundle      = '/opt/bicep-langserver'
      local lsp_bicep_path        = vim.fn.resolve(lsp_bicep_bundle .. '/' .. lsp_bicep_binary)

      --------------------------------------------------------------------------

      local lsp_powershell_bundle = '/opt/powershell-editor-services'

      --------------------------------------------------------------------------

      local lsp_has_bash          = vim.fn.executable('bash-language-server') == 1
      local lsp_has_bicep         = vim.fn.filereadable(lsp_bicep_path) == 1
      local lsp_has_docker        = vim.fn.executable('docker-langserver') == 1
      local lsp_has_eslint        = vim.fn.executable('vscode-eslint-language-server') == 1
      local lsp_has_go            = vim.fn.executable('gopls') == 1
      local lsp_has_helm          = vim.fn.executable('helm_ls') == 1
      local lsp_has_json          = vim.fn.executable('vscode-json-language-server') == 1
      local lsp_has_lua           = vim.fn.executable('lua-language-server') == 1
      local lsp_has_php           = vim.fn.executable('phpactor') == 1
      local lsp_has_powershell    = vim.fn.filereadable(lsp_powershell_bundle ..
        '/PowerShellEditorServices/Start-EditorServices.ps1') == 1
      local lsp_has_python        = vim.fn.executable('pylsp') == 1
      local lsp_has_ruby          = vim.fn.executable('solargraph') == 1
      local lsp_has_terraform     = vim.fn.executable('terraform-ls') == 1
      local lsp_has_typescript    = vim.fn.executable('vtsls') == 1
      local lsp_has_yaml          = vim.fn.executable('yaml-language-server') == 1

      --------------------------------------------------------------------------

      if lsp_has_bash then
        require('lspconfig').bashls.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_bicep then
        require('lspconfig').bicep.setup({
          capabilities = lsp_capabilities,
          cmd          = { 'dotnet', lsp_bicep_path }
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_docker then
        require('lspconfig').dockerls.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_eslint then
        require('lspconfig').eslint.setup({
          capabilities = lsp_capabilities,
          on_attach    = function(_, bufnr)
            vim.api.nvim_create_autocmd('BufWritePre', {
              buffer  = bufnr,
              command = 'EslintFixAll'
            })
          end
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_go then
        require('lspconfig').gopls.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_helm then
        require('lspconfig').helm_ls.setup({
          capabilities = lsp_capabilities,
          settings     = {
            ['helm-ls'] = {
              yamlls = {
                config = {
                  redhat = {
                    telemetry = {
                      enabled = false
                    }
                  }
                },
                enabled = lsp_has_yaml
              }
            }
          }
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_json then
        require('lspconfig').jsonls.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_lua then
        require('lspconfig').lua_ls.setup({
          capabilities = lsp_capabilities,
          settings     = {
            lua = {
              telemetry = {
                enable = false
              }
            }
          }
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_php then
        require('lspconfig').phpactor.setup({
          capabilities = lsp_capabilities,
          init_options = {
            ['language_server_php_cs_fixer.bin']     = '/usr/sbin/php-cs-fixer',
            ['language_server_php_cs_fixer.enabled'] = true
          }
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_powershell then
        require('lspconfig').powershell_es.setup({
          bundle_path  = lsp_powershell_bundle,
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_python then
        require('lspconfig').pylsp.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_ruby then
        require('lspconfig').solargraph.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_terraform then
        require('lspconfig').terraformls.setup({
          capabilities = lsp_capabilities,
          root_dir     = lsp_util.root_pattern('*.tf')
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_typescript then
        require('lspconfig').vtsls.setup({
          capabilities = lsp_capabilities
        })
      end

      --------------------------------------------------------------------------

      if lsp_has_yaml then
        require('lspconfig').yamlls.setup({
          capabilities = lsp_capabilities,
          root_dir     = lsp_util.find_git_ancestor,
          settings     = {
            redhat = {
              telemetry = {
                enabled = false
              }
            }
          }
        })
      end

      --------------------------------------------------------------------------
    end
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
