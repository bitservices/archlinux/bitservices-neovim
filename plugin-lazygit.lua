--------------------------------------------------------------------------------
--- PKGBUILD requires: lazygit
--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'kdheepak/lazygit.nvim',
    cmd = {
      'LazyGit',
      'LazyGitConfig',
      'LazyGitFilter',
      'LazyGitCurrentFile',
      'LazyGitFilterCurrentFile'
    },
    dependencies = {
      'nvim-lua/plenary.nvim'
    },
    keys = {
      { '<leader>g', function() require('lazygit').lazygitfiltercurrentfile() end, desc = 'LazyGit Current File' },
      { '<leader>G', function() require('lazygit').lazygitcurrentfile() end,       desc = 'LazyGit' }
    }
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
