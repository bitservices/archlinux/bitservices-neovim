--------------------------------------------------------------------------------
--- PKGBUILD requires: ttf-firacode-nerd
--------------------------------------------------------------------------------

local function nvim_on_attach(bufnr)
  local api = require('nvim-tree.api')

  local function opts(desc)
    return {
      desc = 'nvim-tree: ' .. desc,
      buffer = bufnr,
      noremap = true,
      silent = true,
      nowait = true
    }
  end

  -- Set default mappings
  api.config.mappings.default_on_attach(bufnr)

  -- Remove unwanted mappings
  vim.keymap.del('n', '<Tab>', { buffer = bufnr })
  vim.keymap.del('n', '<C-x>', { buffer = bufnr })

  -- Custom added mappings
  vim.keymap.set('n', '<Space>', api.node.open.preview, opts('Open Preview'), { buffer = bufnr })
  vim.keymap.set('n', '<C-n>', api.node.open.horizontal, opts('Open: Horizontal Split'), { buffer = bufnr })
end

--------------------------------------------------------------------------------

return {

  ------------------------------------------------------------------------------

  {
    'nvim-tree/nvim-tree.lua',
    dependencies = {
      'nvim-tree/nvim-web-devicons'
    },
    keys = {
      { '<Leader>n', function() require('nvim-tree.api').tree.toggle() end },
      { '<Leader>N', function() require('nvim-tree.api').tree.toggle() end }
    },
    opts = {
      diagnostics = {
        enable = true
      },
      filters = {
        custom = {
          "^\\.git$"
        }
      },
      filesystem_watchers = {
        enable         = true,
        debounce_delay = 100,
        ignore_dirs    = {
          '/.ccls-cache',
          '/.git',
          '/.terraform',
          '/.terragrunt-cache',
          '/.venv',
          '/build',
          '/node_modules',
          '/target'
        }
      },
      git = {
        ignore = false,
        timeout = 5000
      },
      on_attach = nvim_on_attach,
      sync_root_with_cwd = true,
      update_focused_file = {
        enable = true
      },
      view = {
        float = {
          enable = true,
          open_win_config = {
            height = 48,
            width = 78
          }
        }
      }
    }
  }

  ------------------------------------------------------------------------------

}

--------------------------------------------------------------------------------
